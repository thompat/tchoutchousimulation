import scala.util.Using
import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayDeque
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.Seq

import sfml.graphics.*
import sfml.window.*

def max(a : Int, b : Int) = if a <= b then b else a
def min(a : Int, b : Int) = if a <= b then a else b

case class Vec2(var x : Int, var y : Int)

def DistAuCarre(u : Vec2, v : Vec2) : Int = {
    val tmp1 = u.x - v.x
    val tmp2 = u.y - v.y
    (tmp1 * tmp1) + (tmp2 * tmp2)
}

def Dist(u : Vec2, v : Vec2) : Int = {
    val d2 : Double = DistAuCarre(u, v).toDouble
    val d : Double = scala.math.sqrt(d2)
    scala.math.round(d.toFloat)
}

def Milieu(u : Vec2, v : Vec2) : Vec2 = {
    Vec2((u.x+v.x)/2, (u.y+v.y)/2)
}

def Angle(src : Vec2, dst : Vec2) : Double = {
    val x = (dst.x-src.x).toDouble
    val y = (src.y-dst.y).toDouble // Car le vecteur unitaire est inversé
    var theta = 0.0
    if x == 0 then {
        if y > 0 then return 360 - 90
        else return 90
    }
    if x >= 0 then {
        theta = scala.math.atan(y/x)
    }
    else {
        theta = scala.math.Pi + scala.math.atan(y/x)
    }
    theta = theta * 180 / scala.math.Pi
    360 - theta

}