abstract class Tache(val ville : Ville):
    val Nom : String
    val Prix : Prix
    def Effectuer() : Unit
    def Essaie() : Boolean = {
        if Prix.MatchRequirement(ville.Contenu) then {
            Effectuer();
            true
        }
        else {
            false
        }
    }

class TacheConstruireTransport(override val ville : Ville, val TypeTransport : TypeTransport, val Conteneur : Conteneur) extends Tache(ville):
    override val Nom = {
        val transport = Transport(0, TypeTransport, Conteneur, ville, ville.world)
        f"Construire ${transport.Nom}"
    }
    override val Prix = TypeTransport.PrixConstruction
    override def Effectuer() : Unit = {
        if Prix.PayWithErrorDisplay(ville.Contenu, ville.world.display) then {
            TransportNewId = TransportNewId + 1
            val transport = Transport(TransportNewId, TypeTransport, Conteneur, ville, ville.world)
            ville.ProgressionDesTransports.addOne((transport, 0.0))
        }
    }

class TacheEvoluerVille(override val ville : Ville) extends Tache(ville):
    override val Nom = "Faire evoluer ville"
    override val Prix = ville.TypeVille.PrixEvolutions(ville.Niveau)
    override def Effectuer() : Unit = {
        ville.ActionEvoluer()
    }

