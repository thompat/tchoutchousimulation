import scala.util.Using
import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayDeque
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.Seq

import sfml.graphics.*
import sfml.window.*

abstract class EtatTuile(val tuile : Tuile):
    val Nom : String
    val CodeSpriteVille : Int = 0
    val CodeSpriteRail : Int = 0
    def Agir() : Unit
    def Fin() : Unit

class EtatNormal(override val tuile : Tuile) extends EtatTuile(tuile):
    override val Nom = "etat normal"
    override def Agir() : Unit = ()
    override def Fin() : Unit = ()

val ProbEnvahissementParLaNature = 0.0000006
class EnvahiParLaNature(override val tuile : Tuile) extends EtatTuile(tuile):
    override val Nom = "envahie par la nature"
    override val CodeSpriteVille = 2
    override val CodeSpriteRail = 1

    // Le contenu est vidé
    tuile match {
        case ville : Ville => {
            ville.ViderContenu()
        }
        case _ => () 
    }

    override def Agir() : Unit = ()
    override def Fin() : Unit = ()


val ProbEruption : Double = 0.000003
class EnEruption(override val tuile : Tuile) extends EtatTuile(tuile):
    override val Nom : String = "en eruption"

    // Initialisation
    val Animation = tuile.world.display.CreateAnimationEruption(tuile.Position, false)
    val IdAnimation = tuile.world.AjouterAnimation(Animation)

    // Les avions pris dans l'étuption sont détruits
    tuile.ProgressionDesTransports = HashMap() 

    var TempsRestant = Animation.temps
    override def Agir() : Unit = {

        TempsRestant = TempsRestant - TimeStepMinute

        val i = tuile.Position.x/32
        val j = tuile.Position.y/32
        for (tmp <- CaseTouchee) do {
            val i0 = i+tmp._1
            val j0 = j+tmp._2
            if (0 <= i0 && i0 < tuile.world.reseau.Hauteur && 0 <= j0 && j0 < tuile.world.reseau.Largeur) then {
                val voisin = tuile.world.reseau.Grille(i0)(j0)
                if EstInflammable(voisin) then {
                    voisin.ChangerEtatTuile(EnFeu(voisin))
                }
            }
        }

        // Le feu peux s'éteindre de lui même
        if (TempsRestant < 0) then {
            tuile.ChangerEtatTuile(EtatNormal(tuile))
        }

    }

    private val CaseTouchee = Array((1,0),(-1,0),(0,1),(0,-1), (1,1), (-1,-1), (1, -1), (-1, 1))
    override def Fin(): Unit = {
        tuile.world.RetirerAnimation(IdAnimation)
    }

val ProbPropagFeu : Double = 0.001
val ProbDebutFeu : Double = 0.000003
def EstInflammable(tuile : Tuile) : Boolean = {
    tuile.EtatTuile match {
        case _ : EtatNormal => {
            tuile match {
                case _: Ville => return true
                case env : TuileEnvironnement => {
                    env.TypeEnvironnement match {
                        case _ : Foret => return true
                        case _ : Construction => return true
                        case _ => return false
                    }
                }
                case _ => return false
            }
        }
        case _ => return false
    }
    false
}
class EnFeu(override val tuile : Tuile) extends EtatTuile(tuile):
    override val Nom : String = "en feu"

    // Initialisation
    val Animation = tuile.world.display.CreateAnimationFeuxForet(tuile.Position, true)
    val IdAnimation = tuile.world.AjouterAnimation(Animation)

    // On détruit tous les transports présent sur la tuile car ceux-ci prennent feux et sont irrécupérable
    tuile match {
        case ville : Ville => {
            tuile.ProgressionDesTransports = HashMap()
            ville.ViderContenu()
        }
        case env : TuileEnvironnement => {
            env.TypeEnvironnement match {
                case _ : Construction => tuile.ProgressionDesTransports = HashMap()
                case _ => ()
            }
        }
        case _ => () 
    }

    var TempsRestant = 800
    private val Choix = Array((1,0),(-1,0),(0,1),(0,-1))
    override def Agir() : Unit = {

        val i = tuile.Position.x/32
        val j = tuile.Position.y/32

        TempsRestant = TempsRestant - TimeStepMinute

        // Le feu se propage on va donc mettre cela en place
        if (Random.between(0, 1.0) <= ProbPropagFeu*TimeStepMinute.toDouble) then {
            val tmp = Choix(Random.between(0,4))
            val i0 = i+tmp._1
            val j0 = j+tmp._2
            if (0 <= i0 && i0 < tuile.world.reseau.Hauteur && 0 <= j0 && j0 < tuile.world.reseau.Largeur) then {
                val voisin = tuile.world.reseau.Grille(i0)(j0)
                if EstInflammable(voisin) then {
                    voisin.ChangerEtatTuile(EnFeu(voisin))
                }
            }
        }

        // Le feu peux s'éteindre de lui même
        if (TempsRestant < 0) then {
            tuile.ChangerEtatTuile(EnFumee(tuile))
        }

    }

    override def Fin(): Unit = {
        tuile.world.RetirerAnimation(IdAnimation)
    }

class EnFumee(override val tuile : Tuile) extends EtatTuile(tuile):
    override val Nom : String = "en fumee"
    var TempsRestant : Int = 1200
    val Animation = tuile.world.display.CreateAnimationFumee(tuile.Position, true)
    val IdAnimation = tuile.world.AjouterAnimation(Animation)
    override def Agir() : Unit = {
        TempsRestant = TempsRestant - TimeStepMinute
        if (TempsRestant < 0) then {
            tuile match {
                case _ : Ville => tuile.ChangerEtatTuile(EnRuine(tuile))
                case env : TuileEnvironnement =>  {
                    env.TypeEnvironnement match {
                        case _ : Construction => tuile.ChangerEtatTuile(EnRuine(tuile))
                        case _ => tuile.ChangerEtatTuile(EtatNormal(tuile))
                    }
                }
                case _ => tuile.ChangerEtatTuile(EtatNormal(tuile))
            }
            
        }
    }
    override def Fin() : Unit = {
        tuile.world.RetirerAnimation(IdAnimation)
    }

class EnRuine(override val tuile : Tuile) extends EtatTuile(tuile):
    override val Nom : String = "en ruine"
    override val CodeSpriteVille : Int = 1
    override val CodeSpriteRail : Int = 1
    override def Agir() : Unit = ()
    override def Fin() : Unit = ()

abstract class Tuile(val Position : Vec2, val world : World) extends Displayable(world.display) with Cliquable:

    var EtatTuile : EtatTuile = EtatNormal(this)
    def ChangerEtatTuile(NouvEtat : EtatTuile) : Unit = {
        EtatTuile.Fin()
        EtatTuile = NouvEtat
    }

    // Partie cliquable
    def EstClique(Localisation: Vec2): Boolean = {
        var res = false
        Using.Manager { use =>
            val rect : RectangleShape = use(RectangleShape())
            rect.size = (32, 32)
            rect.position = (Position.x, Position.y)
            val bounds : Rect[Float] = rect.globalBounds
            res = bounds.contains(sfml.system.Vector2[Float](Localisation.x.toFloat, Localisation.y.toFloat))
        }.get
        res
    }

    // Partie transport

    var ProgressionDesTransports : HashMap[Transport, Double] = HashMap[Transport, Double]() // Associe à chaque Id de train un score de progression

    def Contient(transport : Transport) : Boolean = { // Retourne true si la portion contient le transport
        ProgressionDesTransports.contains(transport) 
    }
    def FaireArriver(transport : Transport) : Boolean = { // Fait arriver le transport sur la portion et renvoie true si cela réussit, false sinon
        if AutoriseArriver(transport) then {
            ProgressionDesTransports.addOne((transport, 0))
            true
        }
        else {
            false
        }
    }
    def FairePartir(transport : Transport) : Boolean = {
        if !ProgressionDesTransports.contains(transport) then true
        else if AutorisePartir(transport) then {
            ProgressionDesTransports.remove(transport)
            true
        }
        else {
            false
        }
    }

    def AutoriseArriverEtatNormal(transport : Transport) : Boolean
    def AutoriseArriverEtatAnormal(transport : Transport) : Boolean = {
        transport.TypeTransport match {
            case _ : Avion => true
            case _ => false
        }
    }
    def AutoriseArriver(transport : Transport) : Boolean = {
        EtatTuile match {
            case _ : EtatNormal => AutoriseArriverEtatNormal(transport)
            case _ => AutoriseArriverEtatAnormal(transport)
        }
    } // Retourne true si le train est autoriser à venir sur cette portion

    def AutorisePartir(transport : Transport) : Boolean // Retourne true si le train peut partir de la portion

    def TempsMin(transport : Transport) : Double // Donne le temps minimal passé par un train sur la portion
    
    def FaireProgresser(transport : Transport, DistParcouru : Double) : Double  // Augmente le score de progression du train sur la portion s'il est présent en fonction de la Distance Parcouru donner en (m), retourne la progression en trop effectuée par le train

    def getSelector() : SelectorTuile

    def Update() : Unit = {
        ProgressionDesTransports.foreach(e => e._1.Avancer())
        EtatTuile.Agir()
    }






abstract class TuileVille(override val Position : Vec2, override val world : World) extends Tuile(Position, world):

    override def AutoriseArriverEtatNormal(transport: Transport): Boolean = {
        true
    }
    override def AutoriseArriverEtatAnormal(transport: Transport): Boolean = {
        false
    }

    override def AutorisePartir(transport : Transport) : Boolean = {
        if ProgressionDesTransports.contains(transport) then ProgressionDesTransports.apply(transport) >= TempsMin(transport)
        else true
    } // Retourne true si le train peut partir de la portion

    override def TempsMin(transport: Transport): Double = {
        transport.TypeTransport.TempsArretVille
    }

    override def FaireProgresser(transport: Transport, DistParcouru: Double): Double = {
        // On met à jour la progression
        var progression = ProgressionDesTransports.apply(transport)
        if progression < this.TempsMin(transport) then progression = progression + TimeStepMinute.toDouble // + 1 timeStepMinute minute à chaque progression
        ProgressionDesTransports.addOne((transport, progression))
        0.0
    }

    override def getSprite(): Sprite

    override def Draw(): Unit = {
        display.SpritePlaine.position = (Position.x, Position.y)
        display.Window.draw(display.SpritePlaine)
        display.Window.draw(getSprite())
    }






class TuileEnvironnement(val TypeEnvironnement : TypeEnvironnement, override val Position : Vec2, override val world : World) extends Tuile(Position, world):

    override def AutoriseArriverEtatNormal(transport: Transport): Boolean = {
        TypeEnvironnement.TransportsAutorises.contains(transport.TypeTransport)
    }
    override def AutorisePartir(transport : Transport) : Boolean = {
        if ProgressionDesTransports.contains(transport) then ProgressionDesTransports.apply(transport) >= 32.0
        else true
    }
    override def TempsMin(transport: Transport): Double = {
        if AutoriseArriver(transport) then {
            return 32.0/transport.TypeTransport.VitesseMax
        }
        else {
            return Double.PositiveInfinity
        }
    }
    override def FaireProgresser(transport: Transport, DistParcouru: Double): Double = {
        // On met à jour la progression
        var progression = ProgressionDesTransports.apply(transport)
        progression = progression + DistParcouru
        val res = progression - 32.0
        if res > 0.0 then progression = progression - res
        ProgressionDesTransports.addOne((transport, progression))
        res
    }

    override def getSprite(): Sprite = {
        val sprite = TypeEnvironnement.Sprite
        sprite.position = (Position.x, Position.y)
        sprite
    }

    override def Draw(): Unit = {
        TypeEnvironnement.SpriteBackground match {
            case Some(sprite) => {
                sprite.position = (Position.x, Position.y)
                display.Window.draw(sprite)
            }
            case _ => ()
        }
        display.Window.draw(getSprite())
    }

    override def getSelector() : SelectorTuile = {
        SelectorEnvironnement(this)
    }

    override def Update() : Unit = {
        ProgressionDesTransports.foreach(e => e._1.Avancer())
        TypeEnvironnement.Actualise(this)
        EtatTuile.Agir()
    }


abstract class TypeEnvironnement(world : World):

    val Nom : String
    val TransportsAutorises : HashMap[TypeTransport, Boolean]

    var Sprite : Sprite = world.display.SpritePlaine // On initialise n'importe comment
    var SpriteBackground : Option[Sprite] = None

    def getActions(tuile : TuileEnvironnement, world : World) : ArrayBuffer[BoutonTexte]
    def Actualise(tuile : TuileEnvironnement) : Unit = ()


abstract class Construction(world : World) extends TypeEnvironnement(world):

    override def getActions(tuile : TuileEnvironnement, world : World) : ArrayBuffer[BoutonTexte]= {
        ArrayBuffer[BoutonTexte](CreateBoutonDeclencherFeu(tuile, PosBtnHautGauche(1), tuile.world.display))
    }

    val PrixConstruction : Prix
    def Switch(world : World) : Construction


class CheminDeFer(world : World) extends Construction(world):

    override val Nom : String = "Chemin De Fer"
    override val TransportsAutorises : HashMap[TypeTransport, Boolean] = HashMap[TypeTransport, Boolean]((Train(), true), (Avion(), true))
    override val PrixConstruction : Prix = Prix(Seq[Quantite](Quantite(or, 50)))

    Sprite = world.display.SpritesRail(0)(10)
    private def EstCheminDeFer(reseau : Reseau, i : Int, j : Int) : Int = {
        if i < 0 || i >= reseau.Hauteur || j < 0 || j >= reseau.Largeur then {
            return 0
        }
        else {
            reseau.Grille(i)(j) match {
                case env : TuileEnvironnement => {
                    env.TypeEnvironnement match {
                        case _ : CheminDeFer => return 1
                        case _ => return 0
                    }
                }
                case _ : Ville => return 1
                case _ => return 0
            }
        }
    }
    private def getCodeRail(reseau : Reseau, i : Int, j : Int) : Int = {
        var i1 = EstCheminDeFer(reseau, i, j-1)
        var i2 = EstCheminDeFer(reseau, i+1, j)
        var i3 = EstCheminDeFer(reseau, i, j+1)
        var i4 = EstCheminDeFer(reseau, i-1, j)
        return reseau.display.CodeRailOf(i1, i2, i3, i4)
    }

    SpriteBackground = Some(world.display.SpritePlaine)

    override def Switch(world : World) : Construction = { this }

    
    override def getActions(tuile : TuileEnvironnement, world : World) : ArrayBuffer[BoutonTexte]= {
        val BtnReparer = CreateBoutonReparer(tuile, Prix(Seq[Quantite](Quantite(or, 40))), PosBtnHautGauche(1), tuile.world.display)
        val BtnDeclencherFeu = CreateBoutonDeclencherFeu(tuile, PosBtnHautGauche(1), tuile.world.display)
        tuile.EtatTuile match {
            case _: EnRuine => ArrayBuffer[BoutonTexte](BtnReparer)
            case _: EnvahiParLaNature => ArrayBuffer[BoutonTexte](BtnReparer)
            case _: EtatNormal => ArrayBuffer[BoutonTexte](BtnDeclencherFeu)
            case _ => ArrayBuffer[BoutonTexte]()
        }
    }

    override def Actualise(tuile: TuileEnvironnement): Unit = {
        val CodeSprite = getCodeRail(tuile.world.reseau, tuile.Position.x/32, tuile.Position.y/32)
        Sprite = tuile.display.SpritesRail(tuile.EtatTuile.CodeSpriteRail)(CodeSprite)
        tuile.EtatTuile match {
            case _ : EtatNormal => {
                if (Random.between(0.0, 1.0) <= ProbEnvahissementParLaNature*TimeStepMinute.toDouble) then {
                    tuile.ChangerEtatTuile(EnvahiParLaNature(tuile))
                }
            }
            case _ => ()
        }
    }



abstract class Nature(world : World) extends TypeEnvironnement(world):

    override def getActions(tuile : TuileEnvironnement, world : World) : ArrayBuffer[BoutonTexte] = {
        tuile.EtatTuile match {
            case _ : EtatNormal => {
                ArrayBuffer[BoutonTexte](
                    BoutonConstruireVille(world, tuile, PosBtnHautGauche(1), world.display),
                    BoutonConstruireConstruction(world, tuile, PosBtnHautGauche(2), world.display)
                )
            }
            case _ => ArrayBuffer[BoutonTexte]()
        }
    }

    private def estEau(reseau : Reseau, i : Int, j : Int) : Boolean = {
        if i < 0 || i >= reseau.Hauteur || j < 0 || j >= reseau.Largeur then {
            return false
        }
        else {
            reseau.Grille(i)(j) match {
                case env : TuileEnvironnement => {
                    env.TypeEnvironnement match {
                        case _ : Eau => return true
                        case _ => return false
                    }
                }
                case _ => return false
            }
        }
    }
    val TypeVilleAssocie : Option[TypeVille]
    def VilleAConstruire(tuile : TuileEnvironnement) : Option[TypeVille] = {
        val reseau = tuile.world.reseau
        val i = tuile.Position.x/32
        val j = tuile.Position.y/32
        if (estEau(reseau, i, j)) then {
            return None
        }
        else {
            if (estEau(reseau, i-1, j) || estEau(reseau, i+1, j) || estEau(reseau, i, j-1) || estEau(reseau, i, j+1)) then {
                return Some(VillePortuaire(tuile.world))
            }
            else {
                return TypeVilleAssocie
            }
        }
    }

    def Switch(world : World) : Nature

val NbEnvNaturels = 5
def EnvNaturelsByNumber(world : World) = Array[Nature](Eau(world), Plaine(world), Montagne(world), Colline(world), Foret(world))

case class Eau(world : World) extends Nature(world):

    override val Nom : String = "Eau"
    override val TransportsAutorises : HashMap[TypeTransport, Boolean] = HashMap[TypeTransport, Boolean]((Bateau(), true), (Avion(), true))
    Sprite = world.display.SpriteEau
    override def getActions(tuile: TuileEnvironnement, world: World): ArrayBuffer[BoutonTexte] = {
        ArrayBuffer[BoutonTexte]()
    }
    override def Switch(world : World) : Nature = {Plaine(world)}
    override val TypeVilleAssocie : Option[TypeVille] = None

case class Plaine(world : World) extends Nature(world):

    override val Nom : String = "Plaine"
    override val TransportsAutorises : HashMap[TypeTransport, Boolean] = HashMap[TypeTransport, Boolean]((Avion(), true))
    Sprite = world.display.SpritePlaine
    override def Switch(world : World) : Nature = {Montagne(world)}
    override val TypeVilleAssocie : Option[TypeVille] = Some(VilleTechnologique(world))

case class Montagne(world : World) extends Nature(world):

    override val Nom : String = "Montagne"
    override val TransportsAutorises : HashMap[TypeTransport, Boolean] = HashMap[TypeTransport, Boolean]((Avion(), true))
    Sprite = world.display.SpriteMontagne
    override def Switch(world : World) : Nature = {Foret(world)}
    override val TypeVilleAssocie : Option[TypeVille] = Some(VilleMiniere(world))

    override def Actualise(tuile : TuileEnvironnement) : Unit = {
        tuile.EtatTuile match {
            case _ : EtatNormal => {
                if (Random.between(0.0, 1.0) <= ProbEruption*TimeStepMinute.toDouble) then {
                    tuile.ChangerEtatTuile(EnEruption(tuile))
                }
            }
            case _ => ()
        }
    }

case class Foret(world : World) extends Nature(world):

    override val Nom : String = "Foret"
    override val TransportsAutorises : HashMap[TypeTransport, Boolean] = HashMap[TypeTransport, Boolean]((Avion(), true))
    Sprite = world.display.SpriteForet
    override def Switch(world : World) : Nature = {Colline(world)}
    override val TypeVilleAssocie : Option[TypeVille] = Some(VilleScierie(world))

    override def Actualise(tuile : TuileEnvironnement) : Unit = {
        tuile.EtatTuile match {
            case _ : EtatNormal => {
                if (Random.between(0.0, 1.0) <= ProbDebutFeu*TimeStepMinute.toDouble) then {
                    tuile.ChangerEtatTuile(EnFeu(tuile))
                }
            }
            case _ => ()
        }
    }


case class Colline(world : World) extends Nature(world):

    override val Nom : String = "Colline"
    override val TransportsAutorises : HashMap[TypeTransport, Boolean] = HashMap[TypeTransport, Boolean]((Avion(), true))
    Sprite = world.display.SpriteColline
    override def Switch(world : World) : Nature = {Eau(world)}
    override val TypeVilleAssocie : Option[TypeVille] = Some(VilleCarrierre(world))