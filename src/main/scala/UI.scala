import scala.util.Using
import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayDeque
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.Seq

import sfml.graphics.*
import sfml.window.*

abstract class Selector():
    var Boutons : ArrayBuffer[BoutonTexte] = ArrayBuffer[BoutonTexte]()
    def Actualiser(ui : UI) : Unit
    def Afficher(ui : UI) : Unit

    def GererSourisCliqueSansBoutonClique(ui : UI, btn : Mouse.Button, x : Int, y : Int) : Unit = {
        btn match {
            case Mouse.Button.Right => ui.RetirerSelector()
            case _ => ()
        }
    }
    def GererSourisClique(ui : UI, btn : Mouse.Button, x : Int, y : Int) : Unit = {
        var unBtnEstClique = false
        Boutons.foreach(actionBtn => {
            if actionBtn.EstClique(Vec2(x, y)) then {
                unBtnEstClique = true
                btn match {
                    case Mouse.Button.Left => actionBtn.Action()
                    case Mouse.Button.Right => actionBtn.CliqueDroit()
                    case _ => ()
                }
            }
        })
        if !unBtnEstClique then {
            GererSourisCliqueSansBoutonClique(ui, btn, x, y)
        }
    }
    def GererMolette(ui : UI, delta : Float, x : Int, y : Int) : Unit = {
        Boutons.foreach(actionBtn => if actionBtn.EstClique(Vec2(x, y)) then actionBtn.RouletteSouris(delta))
    }

    def GererEvent(ui : UI) : Unit = {
        for event <- ui.world.window.pollEvent() do {
            event match {
                case Event.MouseButtonPressed(btn, x, y) => {
                    GererSourisClique(ui, btn, x, y)
                }
                case Event.MouseWheelScrolled(wheel, delta, x, y) => {
                    GererMolette(ui, delta, x, y)
                }
                case _ => GererEvenementDeBase(event, ui)
            }
        }
    }

abstract class SelectorTuile(val tuile : Tuile) extends Selector()
class SelectorEnvironnement(val environnement : TuileEnvironnement) extends SelectorTuile(environnement):
    override def Actualiser(ui : UI) : Unit = {
        Boutons = environnement.TypeEnvironnement.getActions(environnement, ui.world)
    }
    override def Afficher(ui : UI) : Unit = {
        ui.world.display.textNom.string = f"${environnement.TypeEnvironnement.Nom} - ${environnement.EtatTuile.Nom}"
        ui.world.display.Window.draw(ui.world.display.textNom)
        Boutons.foreach(btn => btn.Draw())
    }


class SelectorVille(val ville : Ville) extends SelectorTuile(ville):
    
    // Définition des boutons de base 
    val BoutonsDeBase : ArrayBuffer[BoutonTexte] = ArrayBuffer[BoutonTexte](
        BoutonFaireEvoluerVille(ville, PosBtnHautGauche(1), ville.display),
        BoutonVendreRessource(ville, PosBtnHautGauche(2), ville.display)
    )
    if !(ville.TypeVille.TransportsConstructibles.isEmpty) then {
        BoutonsDeBase.addOne(BoutonConstruireTransport(ville, PosBtnHautGauche(3), ville.display))
    }
    BoutonsDeBase.addOne(CreateBoutonDeclencherFeu(ville, PosBtnHautGauche(4), ville.world.display))

    val prixReparation : Int = (ville.TypeVille.PrixEvolutions(0).GetPrix(or).toDouble * 0.8).toInt
    val BtnReparer : BoutonTexte = CreateBoutonReparer(ville, Prix(Seq[Quantite](Quantite(or, prixReparation))), PosBtnHautGauche(1), tuile.world.display)

    def ActualiserEtatNormal(ui : UI) : Unit = {
        Boutons = ArrayBuffer[BoutonTexte]()
        BoutonsDeBase.foreach(btn => Boutons.addOne(btn))
        var i = BoutonsDeBase.length+1
        ville.ProgressionDesTransports.foreach(e => {
            Boutons.addOne(BoutonSelectionnerTransport(ui, e._1, PosBtnHautGauche(i), ui.world.display));
            i = i + 1
        })
    }

    def ActualiserEnRuine(ui : UI) : Unit = {
        Boutons = ArrayBuffer[BoutonTexte](BtnReparer)
    }

    def ActualiserEnvahieParLaNature(ui : UI) : Unit = {
        Boutons = ArrayBuffer[BoutonTexte](BtnReparer)
    }

    Boutons = BoutonsDeBase
    override def Actualiser(ui : UI) : Unit = {
        
        ville.EtatTuile match {
            case _ : EtatNormal => ActualiserEtatNormal(ui)
            case _ : EnRuine => ActualiserEnRuine(ui)
            case _ : EnvahiParLaNature => ActualiserEnvahieParLaNature(ui)
            case _ => Boutons = ArrayBuffer[BoutonTexte]()
        }
        
    }

    override def Afficher(ui : UI) : Unit = {

        var i = 1
        for (quantiteType <- ville.Contenu.Types) {
            ui.world.display.DrawInformationRessource(i, quantiteType.Sprite(ui.world.display), f"${ville.Contenu.Lookup(quantiteType)}")
            i = i + 1
        }

        ui.world.display.textNom.string = f"${ville.TypeVille.Nom} de niv. ${ville.Niveau} - ${ville.EtatTuile.Nom}"
        ui.world.display.Window.draw(ui.world.display.textNom)
        Boutons.foreach(btn => btn.Draw())

        val point = ville.display.pointSelecteurVille
        val pos = ville.Position
        point.position = (pos.x, pos.y)
        ville.display.Window.draw(point)

    }

class SelectorTransport(val transport : Transport) extends Selector():

    val BtnActiverModeAuto = BoutonAction(() => {transport.ModeAuto = true;}, "Activer le mode auto", PosBtnHautGauche(1), transport.world.display)
    val BtnDesactiverModeAuto = BoutonAction(() => {transport.ModeAuto = false;}, "Desactiver le mode auto", PosBtnHautGauche(1), transport.world.display)

    Boutons = ArrayBuffer[BoutonTexte](BtnActiverModeAuto)

    override def Actualiser(ui : UI) : Unit = {

        // Gestion du mode auto
        if transport.ModeAuto then {
            Boutons.update(0,BtnDesactiverModeAuto)
        }
        else {
            Boutons.update(0,BtnActiverModeAuto)
        }

        // Boutons d'échange avec une ville
        transport.Localisation match {
            case ville : Ville => {
                if Boutons.length == 1 then {
                    var i = 1
                    for (quantiteType <- transport.Contenu.Types) {
                        val btn = EchangeQuantiteTransportVille(Quantite(quantiteType, 1), ville, transport, PosBtnHautDroite(i), ui.world.display)
                        Boutons.addOne(btn)
                        i = i + 1
                    }
                }
            }
            case _ => {
                Boutons = ArrayBuffer[BoutonTexte](Boutons.apply(0))
            }
        }
    }
    override def Afficher(ui : UI): Unit = {

        transport.Localisation match {
            case ville : Ville => {
                var i = 1
                for (quantiteType <- transport.Contenu.Types) {
                    ui.world.display.DrawInformationRessource(i, quantiteType.Sprite(ui.world.display), "")
                    i = i + 1
                }
            }
            case _ => {
                var i = 1
                for (quantiteType <- transport.Contenu.Types) {
                    ui.world.display.DrawInformationRessource(i, quantiteType.Sprite(ui.world.display), f"${transport.Contenu.Lookup(quantiteType)}")
                    i = i + 1
                }
            }
        }

        // Affichage des actions
        ui.world.display.textNom.string = transport.Nom
        ui.world.display.Window.draw(ui.world.display.textNom)
        Boutons.foreach(btn => btn.Draw())

    }

    override def GererSourisCliqueSansBoutonClique(ui: UI, btn: Mouse.Button, x: Int, y: Int): Unit = {
        btn match {
            case Mouse.Button.Right => ui.RetirerSelector()
            case Mouse.Button.Left => {
                for (i <- 0 to (ui.world.reseau.Hauteur-1).toInt) {
                    for (j <- 0 to (ui.world.reseau.Largeur-1).toInt) {
                        val tuile = ui.world.reseau.Grille(i)(j)
                        tuile match {
                            case ville : Ville => {
                                if ville.EstClique(Vec2(x, y)) then {
                                    val trajet = ui.world.reseau.Dijkstra(transport, transport.Localisation, ville)
                                    if trajet._2 != Double.PositiveInfinity then {
                                        transport.Destinations = trajet._1
                                    }
                                }
                            }
                            case _ => ()
                        }
                                        
                    }
                }
            }
            case _ => ()
        }
    }

class SelectorTableauTaches(val world : World) extends Selector():
    override def Actualiser(ui : UI) : Unit = {
        var i = 0
        Boutons = ArrayBuffer[BoutonTexte](BoutonRetour(ui, PosBtnHautGauche(i), world.display))
        i = i + 1
        world.TableauTaches.foreach(t => {
            Boutons.addOne(BoutonSelectionner(ui, SelectorTache(world, t, this), t.Nom, PosBtnHautGauche(i), world.display))
            i =  i + 1
        })
    }
    override def Afficher(ui : UI): Unit = {
        Boutons.foreach(btn => btn.Draw())
    }

class SelectorTache(val world : World, val tache : Tache, val tableauTaches : SelectorTableauTaches) extends Selector():
    var actif : Boolean = false
    var ligne : Int = 0
    override def Actualiser(ui : UI) : Unit = {
        tableauTaches.Actualiser(ui)
        if world.IndexTableauTaches.contains(tache) then {
            actif = true
            ligne = world.IndexTableauTaches.apply(tache)
        }
        else {
            actif = false
        }
    }
    override def Afficher(ui: UI): Unit = {
        if actif then {

            val point = world.display.pointSelecteurVille
            val pos = tache.ville.Position
            point.position = (pos.x, pos.y)
            world.window.draw(point)

            tableauTaches.Afficher(ui)

            val txt = world.display.textTacheHighlight
            txt.position = SlotHautGauche(ligne+1)
            txt.string = tache.Nom
            world.window.draw(txt)

            tache.Prix.MatchRequirementWithErrorDisplay(tache.ville.Contenu, world.display)

            var i = 1
            for (quantiteType <- tache.ville.Contenu.Types) {
                ui.world.display.DrawInformationRessource(i, quantiteType.Sprite(ui.world.display), f"${tache.ville.Contenu.Lookup(quantiteType)}")
                i = i + 1
            }
            
        }
    }
    override def GererEvent(ui : UI) : Unit = {
        if actif then {
        for event <- ui.world.window.pollEvent() do {
            event match {
                case Event.MouseButtonPressed(btn, x, y) => {
                    var unBtnEstClique = false
                    tableauTaches.Boutons.foreach(actionBtn => {
                        if actionBtn.EstClique(Vec2(x, y)) then {
                            unBtnEstClique = true
                            btn match {
                                case Mouse.Button.Left => {
                                    ui.RetirerSelector()
                                    actionBtn.Action()
                                }
                                case _ => ()
                            }
                        }
                    })
                    if !unBtnEstClique then {
                        btn match {
                                case Mouse.Button.Right => {
                                    ui.RetirerSelector()
                                }
                                case _ => ()
                            }
                    }
                }
                case Event.MouseWheelScrolled(wheel, delta, x, y) => {
                    if delta < 0 then {
                        // On descend la tache dans la liste
                        if (ligne < world.TableauTaches.length-1) {
                            world.TableauTaches.update(ligne, world.TableauTaches.apply(ligne+1))
                            world.TableauTaches.update(ligne+1, tache)
                            world.UpdateIndexTableauTaches()
                        }
                    }
                    else {
                        if (ligne > 0) {
                            world.TableauTaches.update(ligne, world.TableauTaches.apply(ligne-1))
                            world.TableauTaches.update(ligne-1, tache)
                            world.UpdateIndexTableauTaches()
                        }
                    }

                }
                case Event.KeyPressed(key, _, _, _, _) => {
                    key match {
                        case Keyboard.Key.KeyDelete | Keyboard.Key.KeyBackspace => {
                            world.TableauTaches.remove(ligne)
                            actif = false
                            world.UpdateIndexTableauTaches()
                        }
                        case _ => ()
                    }
                }
                case _ => GererEvenementDeBase(event, ui)
            }
        }
        }
        else {
            ui.RetirerSelector()
        }
        
    }

class UI(val world : World):
    var SelectorStack : List[Selector] = List[Selector]()
    def RetirerSelector() : Unit = {
        SelectorStack match {
            case Nil => ()
            case _::others => SelectorStack = others
        }
    }
    def AddSelector(selector : Selector) : Unit = {
        SelectorStack = selector::SelectorStack
    }

    val BoutonTableauTaches : BoutonTexte = BoutonSelectionner(this, SelectorTableauTaches(world), "Taches en attentes", PosBtnHautGauche(0), world.display)
    private def Afficher() : Unit = {
        BoutonTableauTaches.Draw()
    }
    private def GererEvent() : Unit = {
        for event <- world.window.pollEvent() do {
            event match {

                case Event.MouseButtonPressed(btn, x, y) => {
                    btn match {
                        case Mouse.Button.Left => {
                            if BoutonTableauTaches.EstClique(Vec2(x, y)) then {
                                BoutonTableauTaches.Action()
                            }
                            else {
                                // On regarde quelle tuile est cliquée
                                for (i <- 0 to (world.reseau.Hauteur-1).toInt) {
                                    for (j <- 0 to (world.reseau.Largeur-1).toInt) {
                                        val tuile = world.reseau.Grille(i)(j)
                                        if tuile.EstClique(Vec2(x, y)) then {
                                            AddSelector(tuile.getSelector())
                                            tuile match {
                                                case ville : Ville => ()
                                                case _ => {
                                                    // Si ce n'est pas une ville et qu'il y a des transports dessus on les selectionnent par dessus
                                                    if !tuile.ProgressionDesTransports.isEmpty then {
                                                        tuile.ProgressionDesTransports.foreach((transport, _) => AddSelector(SelectorTransport(transport)))
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                        case _ => ()
                    }
                }

                case _ => GererEvenementDeBase(event, this)
            }
        }
    }

    def Interagir() : Unit = {
        SelectorStack match {
            case Nil => {
                Afficher()
                GererEvent()
            }
            case selector::selectors => {
                selector.Actualiser(this)
                selector.Afficher(this)
                selector.GererEvent(this)
            }
        }
    }

def GererEvenementDeBase(event : Event, ui : UI) : Unit = {
    event match {
        // Base case events
        case Event.Closed() => ui.world.window.close()
        case Event.Resized(width, height) => ui.world.window.view = View((0, 0, width, height))
        case _ => ()
    }
}