import scala.util.Using
import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayDeque
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.Seq

import sfml.graphics.*
import sfml.window.*

var Argent = 1100

trait TypeQuantite:
    val Nom : String
    def FacteurPrix(ville : Ville) : Int
    def Sprite(display : Display) : Sprite

case class Quantite(var t : TypeQuantite, var q : Int)

trait Consommable extends TypeQuantite:
    val Nom : String
    def FacteurPrix(ville : Ville) : Int
    def Sprite(display : Display) : Sprite

trait Ressource extends Consommable:
    val Nom : String
    def FacteurPrix(ville : Ville) : Int
    def Sprite(display : Display) : Sprite
    def Switch() : Ressource

trait Special extends Consommable:
    val Nom : String
    def FacteurPrix(ville : Ville) : Int
    def Sprite(display : Display) : Sprite

trait Vivant extends TypeQuantite:
    val Nom : String
    def FacteurPrix(ville : Ville) : Int
    def Sprite(display : Display) : Sprite



case class Humain() extends Vivant:
    val Nom = "humain"
    def FacteurPrix(ville : Ville) : Int = 0
    def Sprite(display : Display) : Sprite = {
        display.SpritePopulation
    }
val humain = Humain()
case class Or() extends Special:
    val Nom = "or"
    def FacteurPrix(ville : Ville) : Int = 1
    def Sprite(display : Display) : Sprite = {
        display.SpriteArgent
    }
val or = Or()
case class Technologie() extends Ressource: 
    val Nom = "technologie"
    def FacteurPrix(ville : Ville) : Int = {
        ville.TypeVille match {
            case _ : VilleTechnologique => 1
            case _ => 2
        }
    }
    def Sprite(display : Display) : Sprite = {
        display.SpriteTechnologie
    }
    def Switch() : Ressource = {
        bois
    }
val technologie = Technologie()
case class Bois() extends Ressource:
    val Nom = "bois"
    def FacteurPrix(ville : Ville) : Int = {
        ville.TypeVille match {
            case _ : VilleScierie => 1
            case _ => 2
        }
    }
    def Sprite(display : Display) : Sprite = {
        display.SpriteBois
    }
    def Switch() : Ressource = {
        pierre
    }
val bois = Bois()
case class Pierre() extends Ressource:
    val Nom = "pierre"
    def FacteurPrix(ville : Ville) : Int = {
        ville.TypeVille match {
            case _ : VilleCarrierre => 1
            case _ => 2
        }
    }
    def Sprite(display : Display) : Sprite = {
        display.SpritePierre
    }
    def Switch() : Ressource = {
        fer
    }
val pierre = Pierre()
case class Fer() extends Ressource:
    val Nom = "fer"
    def FacteurPrix(ville : Ville) : Int = {
        ville.TypeVille match {
            case _ : VilleMiniere => 1
            case _ => 2
        }
    }
    def Sprite(display : Display) : Sprite = {
        display.SpriteFer
    }
    def Switch() : Ressource = {
        poisson
    }
val fer = Fer()
case class Poisson() extends Ressource :
    val Nom = "poisson"
    def FacteurPrix(ville : Ville) : Int = {
        ville.TypeVille match {
            case _ : VillePortuaire => 1
            case _ => 2
        }
    }
    def Sprite(display : Display) : Sprite = {
        display.SpritePoisson
    }
    def Switch() : Ressource = {
        technologie
    }
val poisson = Poisson()

def PrixRessource(ressource : Quantite, ville : Ville) : Int = {
    ressource.t.FacteurPrix(ville)*ressource.q
}

val QuantiteSeqTout = Seq[TypeQuantite](humain, or, technologie, bois, pierre, fer, poisson)
val QuantiteSeqConsommables = Seq[TypeQuantite](or, technologie, bois, pierre, fer, poisson)
val QuantiteSeqRessources = Seq[TypeQuantite](technologie, bois, pierre, fer, poisson)
val QuantiteSeqVivants = Seq[TypeQuantite](humain)


class Conteneur(capacites : Seq[Quantite]):

    val Types = {
        val tmp = ArrayBuffer[TypeQuantite]()
        capacites.foreach(quantite => tmp.addOne(quantite.t))
        tmp.toSeq
    }
    val Capacites = HashMap[String, Int]()
    val Contenus = HashMap[String, Int]()
    for (Quantite <- capacites) {
        val n = Quantite.q
        Capacites.addOne((Quantite.t.Nom, n))
        Contenus.addOne((Quantite.t.Nom, 0))
    }

    def Autoriser(typeQuantite : TypeQuantite) : Boolean = {
        Capacites.contains(typeQuantite.Nom)
    }

    def Lookup(typeQuantite : TypeQuantite) : Int = {
        if (Autoriser(typeQuantite)) {
            Contenus.apply(typeQuantite.Nom)
        }
        else {
            0
        }
    }
    def LookupCapacite(typeQuantite : TypeQuantite) : Int = {
        if (Autoriser(typeQuantite)) {
            Capacites.apply(typeQuantite.Nom)
        }
        else {
            0
        }
    }
    def Retirer(Quantite : Quantite) : Int = {
        if (Autoriser(Quantite.t)) {
            val q = Quantite.q
            val n = Contenus.apply(Quantite.t.Nom)
            Contenus.addOne((Quantite.t.Nom, n - min(n, q)))
            min(n, q)
        }
        else {
            0
        }
    }
    def Ajouter(Quantite : Quantite) : Int = {
        if (Autoriser(Quantite.t)) {
            val q = Quantite.q
            val n = Contenus.apply(Quantite.t.Nom)
            val c = Capacites.apply(Quantite.t.Nom)
            Contenus.addOne((Quantite.t.Nom, n + min(c-n, q)))
            min(c-n, q)
        }
        else {
            0
        }
    }
    def Ajouter(Conteneur : Conteneur) : Unit = {
        for (typeQuantite <- Conteneur.Types){
            var contenu = Conteneur.Lookup(typeQuantite)
            contenu = Ajouter(Quantite(typeQuantite, contenu))
            Conteneur.Retirer(Quantite(typeQuantite, contenu))
        }
    }
    def Check(Quantite : Quantite) : Boolean = {
        Quantite.q <= Lookup(Quantite.t)
    }
    def Print() : Unit = {
        println("Contenus : ")
        Capacites.foreach((t, q) => println(f"${t} : ${q}"))
        println("-----------")
    }

class ConteneurHumain(cap : Int) extends Conteneur(QuantiteSeqVivants.flatMap(t => Some(Quantite(t, cap))))
class ConteneurRessources(cap : Int) extends Conteneur(QuantiteSeqRessources.flatMap(t => Some(Quantite(t, cap))))

case class Prix(val Prices : Seq[Quantite]):

    val Types = {
        val tmp = ArrayBuffer[TypeQuantite]()
        Prices.foreach(quantite => tmp.addOne(quantite.t))
        tmp.toSeq
    }

    def GetPrix(typeQuantite : TypeQuantite) : Int = {
        for (prix <- Prices) {
            if prix.t.Nom == typeQuantite.Nom then return prix.q
        }
        return 0
    }

    def MatchRequirement(Contenu : Conteneur) : Boolean = {
        var res = true
        for (prix <- Prices) {
            prix.t match {
                case _ : Or => res = res && prix.q <= Argent
                case _ => res = res && Contenu.Check(prix)
            }
        }
        res
    }
    def MatchRequirementWithErrorDisplay(Contenu : Conteneur, display : Display) : Boolean = {
        var res = true
        var infos = ""

        for (prix <- Prices) {
            prix.t match {
                case _ : Or => {
                    if prix.q > Argent then {
                        res = false
                        infos = infos + f"${prix.q} or requis\n"
                    }
                }
                case _ => {
                    if !(Contenu.Check(prix)) then {
                        res = false
                        infos = infos + f"${prix.q} ${prix.t.Nom}s requis\n"
                    }
                }
            }
        }
        
        if res then {
            display.PrintInfo("Succes !")
        }
        else {
            display.PrintInfo("Echec : \n"+infos)
        }
        res
    }

    def PrixRestant(Contenu : Conteneur) : Prix = {
        var arr = ArrayBuffer[Quantite]()
        for (prix <- Prices) {
            var tmp = min(Contenu.Lookup(prix.t), prix.q)
            tmp = prix.q-tmp
            arr.addOne(Quantite(prix.t, tmp))
        }
        return Prix(arr.toSeq)
    }

    def Pay(Contenu : Conteneur) : Boolean = {
        if MatchRequirement(Contenu) then {
            for (prix <- Prices) {
                prix.t match {
                    case _ : Or => Argent = Argent - prix.q
                    case _ : Ressource => Contenu.Retirer(prix)
                    case _ => ()
                }
            }
            true
        }
        else {
            false
        }
    }
    def PayWithErrorDisplay(Contenu : Conteneur, display : Display) : Boolean = {
        if MatchRequirementWithErrorDisplay(Contenu, display) then {
            for (prix <- Prices)  {
                prix.t match {
                    case _ : Or => Argent = Argent - prix.q
                    case _ : Ressource => Contenu.Retirer(prix)
                    case _ => ()
                }
            }
            true
        }
        else {
            false
        }
    }