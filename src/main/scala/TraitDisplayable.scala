import scala.util.Using
import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayDeque
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.Seq

import sfml.graphics.*
import sfml.window.*

val RESOURCES_DIR = "src/main/resources/"
val FileRessources = "src/main/resources/"


val EspacementHauteurTexte = 22
val TailleTexte = 18
val Intercalere = (32-TailleTexte)/2

def SlotHautGauche(ligne : Int) : (Int, Int) = (0, (ligne)*EspacementHauteurTexte)
def PosBtnHautGauche(ligne : Int) : Vec2 = Vec2(0, (ligne)*EspacementHauteurTexte)

val ColSlotSymbol = 22
def SlotSymbHautDroite(ligne : Int) : (Int, Int) = (ColSlotSymbol*32, ligne*32)
def SlotTxtHautDroite(ligne : Int) : (Int, Int) = ((ColSlotSymbol+1)*32, ligne*32+Intercalere)
def PosBtnHautDroite(ligne : Int) : Vec2 = Vec2((ColSlotSymbol+1)*32, ligne*32+Intercalere)

class Display(val Window : RenderWindow, val use : Using.Manager):

    // Utiliser dans les fonctions
    val textures = ArrayBuffer[Texture]()

    // Font

    val font = use(Font())
    if !(font.loadFromFile("src/main/resources/myFont.ttf")) then System.exit(1)

    // Environnements

    val TextureEau = use(Texture())
    if !(TextureEau.loadFromFile(FileRessources+"Images/Environnements/water.png")) then System.exit(1)
    val TexturePlaine = use(Texture())
    if !(TexturePlaine.loadFromFile(FileRessources+"Images/Environnements/grass.png")) then System.exit(1)
    val TextureColline = use(Texture())
    if !(TextureColline.loadFromFile(FileRessources+"Images/Environnements/hill.png")) then System.exit(1)
    val TextureMontagne = use(Texture())
    if !(TextureMontagne.loadFromFile(FileRessources+"Images/Environnements/montain.png")) then System.exit(1)
    val TextureForet = use(Texture())
    if !(TextureForet.loadFromFile(FileRessources+"Images/Environnements/forest.png")) then System.exit(1)

    val SpriteEau = Sprite(TextureEau)
    val SpritePlaine = Sprite(TexturePlaine)
    val SpriteColline = Sprite(TextureColline)
    val SpriteMontagne = Sprite(TextureMontagne)
    val SpriteForet = Sprite(TextureForet)

    // Rails

    def ChargerRails(extension : String) : ArrayBuffer[Sprite] = {
        
        val res = ArrayBuffer[Sprite]()

        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/0011" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/0101" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/0110" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/0111" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/1001" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/1010" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/1011" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/1100" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/1101" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/1110" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Rails/1111" + extension + ".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))

        res
    }

    // Code normal : 0
    // Code vine : 1
    val SpritesRail = ArrayBuffer[ArrayBuffer[Sprite]](
        ChargerRails(""),
        ChargerRails("_vine")
    )

    def CodeRailOf(i1 : Int, i2 : Int, i3 : Int, i4 : Int) : Int = {
        if (i1==0 && i2==0 && i3==1 && i4==1) then 0
        else if (i1==0 && i2==0 && i3==1 && i4==1) then 0
        else if (i1==0 && i2==1 && i3==0 && i4==1) then 1
        else if (i1==0 && i2==1 && i3==1 && i4==0) then 2
        else if (i1==0 && i2==1 && i3==1 && i4==1) then 3
        else if (i1==1 && i2==0 && i3==0 && i4==1) then 4
        else if (i1==1 && i2==0 && i3==1 && i4==0) then 5
        else if (i1==1 && i2==0 && i3==1 && i4==1) then 6
        else if (i1==1 && i2==1 && i3==0 && i4==0) then 7
        else if (i1==1 && i2==1 && i3==0 && i4==1) then 8
        else if (i1==1 && i2==1 && i3==1 && i4==0) then 9
        else 10

    }

    // Ressources

    val TextureArgent = use(Texture())
    if !(TextureArgent.loadFromFile(FileRessources+"Images/Ressources/coin.png")) then System.exit(1)
    val TexturePoisson = use(Texture())
    if !(TexturePoisson.loadFromFile(FileRessources+"Images/Ressources/fish.png")) then System.exit(1)
    val TextureTechnologie = use(Texture())
    if !(TextureTechnologie.loadFromFile(FileRessources+"Images/Ressources/technology.png")) then System.exit(1)
    val TexturePierre = use(Texture())
    if !(TexturePierre.loadFromFile(FileRessources+"Images/Ressources/stone.png")) then System.exit(1)
    val TextureFer = use(Texture())
    if !(TextureFer.loadFromFile(FileRessources+"Images/Ressources/iron.png")) then System.exit(1)
    val TextureBois = use(Texture())
    if !(TextureBois.loadFromFile(FileRessources+"Images/Ressources/wood.png")) then System.exit(1)
    val TexturePopulation = use(Texture())
    if !(TexturePopulation.loadFromFile(FileRessources+"Images/Ressources/joy.png")) then System.exit(1)

    val SpriteArgent = Sprite(TextureArgent)
    val SpritePopulation = Sprite(TexturePopulation)
    val SpritePoisson = Sprite(TexturePoisson)
    val SpriteTechnologie = Sprite(TextureTechnologie)
    val SpritePierre = Sprite(TexturePierre)
    val SpriteFer = Sprite(TextureFer)
    val SpriteBois = Sprite(TextureBois)

    // Transports

    val TextureTrain = use(Texture())
    if !(TextureTrain.loadFromFile(FileRessources+"Images/Transports/train.png")) then System.exit(1)
    val TextureBateau = use(Texture())
    if !(TextureBateau.loadFromFile(FileRessources+"Images/Transports/boat.png")) then System.exit(1)
    val TextureAvion = use(Texture())
    if !(TextureAvion.loadFromFile(FileRessources+"Images/Transports/plane.png")) then System.exit(1)

    val SpriteTrain = Sprite(TextureTrain)
    val SpriteBateau = Sprite(TextureBateau)
    val SpriteAvion = Sprite(TextureAvion)

    val TextureAvion1 = use(Texture())
    if !(TextureAvion1.loadFromFile(FileRessources+"Images/Transports/plane1.png")) then System.exit(1)
    val TextureAvion2 = use(Texture())
    if !(TextureAvion2.loadFromFile(FileRessources+"Images/Transports/plane2.png")) then System.exit(1)
    val TextureAvion3 = use(Texture())
    if !(TextureAvion3.loadFromFile(FileRessources+"Images/Transports/plane3.png")) then System.exit(1)
    val SpritesAvion = ArrayBuffer[Sprite](SpriteAvion, Sprite(TextureAvion1), Sprite(TextureAvion2), Sprite(TextureAvion3))

    val TextureBateau1 = use(Texture())
    if !(TextureBateau1.loadFromFile(FileRessources+"Images/Transports/boat1.png")) then System.exit(1)
    val SpriteBateau1 = Sprite(TextureBateau1)
    val SpritesBateau = ArrayBuffer[Sprite](SpriteBateau, SpriteBateau1, SpriteBateau1, SpriteBateau)

    val TextureTrain1 = use(Texture())
    if !(TextureTrain1.loadFromFile(FileRessources+"Images/Transports/train1.png")) then System.exit(1)
    val SpriteTrain1 = Sprite(TextureTrain1)
    val SpritesTrain = ArrayBuffer[Sprite](SpriteTrain1, SpriteTrain, SpriteTrain, SpriteTrain1)

    // Villes
    // Code normal : 0
    // Code ruine : 1
    // Code vine : 2

    def ChargerSpritesVilles(nom_ville : String) : ArrayBuffer[Sprite] = {
        val res = ArrayBuffer[Sprite]()
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Villes/"+nom_ville+".png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Villes/"+nom_ville+"_ruine.png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))
        if !(textures.apply(textures.length-1).loadFromFile(FileRessources+"Images/Villes/"+nom_ville+"_vine.png")) then System.exit(1)
        res.addOne(Sprite(textures.apply(textures.length-1)))
        textures.addOne(use(Texture()))

        res
    }

    val SpritesVillePortuaire = ChargerSpritesVilles("City_water")
    val SpritesVilleTechnologique = ChargerSpritesVilles("City_grass")
    val SpritesVilleCarriere = ChargerSpritesVilles("City_hill")
    val SpritesVilleMiniere = ChargerSpritesVilles("City_mountain")
    val SpritesVilleScierie = ChargerSpritesVilles("City_forest")
    

    // Affichage des ressources pour les villes

    val text = use(Text())
    text.font = font
    text.fillColor = Color.White()
    text.characterSize = TailleTexte

    def DrawInformationRessource(ligne : Int, sprite : Sprite, texte : String) : Unit = {
        text.position = SlotTxtHautDroite(ligne)
        text.string = texte
        sprite.position = SlotSymbHautDroite(ligne)
        Window.draw(text)
        Window.draw(sprite)
    }

    val textTransportDisponible = use(Text())
    textTransportDisponible.string = "Transports :"
    textTransportDisponible.position = (22*32, 7*32)
    textTransportDisponible.font = font
    textTransportDisponible.characterSize = TailleTexte+1
    textTransportDisponible.fillColor = Color.White()

    val textNom = use(Text())
    textNom.position = (0, 0)
    textNom.font = font
    textNom.characterSize = TailleTexte+2
    textNom.fillColor = Color.White()

    val textInfo = use(Text())
    textInfo.string = "Info are printed here"
    textInfo.position = (0, 700)
    textInfo.font = font
    textInfo.characterSize = TailleTexte
    textInfo.fillColor = Color.White()

    def PrintInfo(info : String) : Unit = {
        textInfo.string = info
    }
    def AfficherInfo() : Unit = {
        Window.draw(textInfo)
    }

    // Selection des taches
    val textTacheHighlight = use(Text())
    textTacheHighlight.font = font
    textTacheHighlight.fillColor = Color.Red()
    textTacheHighlight.characterSize = TailleTexte

    val pointSelecteurVille = use(CircleShape(5))
    pointSelecteurVille.fillColor = Color.Red()


    // Chargement des animations
    def ChargerFramesAnimation(chemin : String, extension : String, fst : Int, lst : Int, size : Int, final_size : Int) : ArrayBuffer[Sprite] = {
        val res = ArrayBuffer[Sprite]()
        for (i <- fst to lst) do {
            val text = use(Texture())
            if !(text.loadFromFile(chemin+f"${i}."+extension)) then System.exit(1)
            val sprite = Sprite(text)
            sprite.scale = (final_size.toDouble/size.toDouble, final_size.toDouble/size.toDouble)
            res.addOne(sprite)
        }
        res
    }

    val AnimationAccidentTechno = ChargerFramesAnimation(FileRessources+"Animations/accident_technologique/Explosion_", "png", 1, 10, 615, 96)
    val AnimationEruption = ChargerFramesAnimation(FileRessources+"Animations/eruption/Explosion_", "png", 1, 10, 680, 96)
    val AnimationExplosion = ChargerFramesAnimation(FileRessources+"Animations/explosion/", "png", 1, 8, 32, 32)
    val AnimationFeux = ChargerFramesAnimation(FileRessources+"Animations/feux/1_", "png", 0, 119, 100, 96)
    val AnimationFumee = ChargerFramesAnimation(FileRessources+"Animations/fumee/Smoke", "png", 1, 19, 80, 96)
    val AnimationFeuxForet = ChargerFramesAnimation(FileRessources+"Animations/feux_foret/1_", "png", 0, 119, 100, 128)

    def CreateAnimationAccidentTechno(pos : Vec2, boucler : Boolean) : Animation = Animation(AnimationAccidentTechno, Vec2(pos.x-32, pos.y-32-18), 40, boucler)
    def CreateAnimationEruption(pos : Vec2, boucler : Boolean) : Animation = Animation(AnimationEruption, Vec2(pos.x-32, pos.y-2*32-6), 60, boucler)
    def CreateAnimationExplosion(pos : Vec2, boucler : Boolean) : Animation = Animation(AnimationExplosion, pos, 20, boucler)
    def CreateAnimationFeux(pos : Vec2, boucler : Boolean) : Animation = Animation(AnimationFeux, Vec2(pos.x-28, pos.y-32-8), 80, boucler)
    def CreateAnimationFumee(pos : Vec2, boucler : Boolean) : Animation = Animation(AnimationFumee, Vec2(pos.x-32, pos.y-66), 80, boucler)
    def CreateAnimationFeuxForet(pos : Vec2, boucler : Boolean) : Animation = Animation(AnimationFeuxForet, Vec2(pos.x-32-24, pos.y-32-24), 80, boucler)
    

    

class Animation(val frames : ArrayBuffer[Sprite], val Position : Vec2, val temps : Int /* en minute du jeu */, val boucler : Boolean):
    var progression : Double = 0.0
    var ended : Boolean = false
    def Next() : Unit = {
        if progression >= 1.0 && boucler then {
            progression = 0.0
        }
        else if progression < 1.0 then {
            progression = progression + TimeStepMinute.toDouble / temps.toDouble /* timestep * nbfois = temps => 1/nbfois = timestep / temsp*/
        }
        else {
            ended = true
        }
        if (progression > 1.0) then progression = 1.0 else ()
    }
    def Draw(window : RenderWindow) : Unit = {
        
        if !ended && progression <= 1.0 then {
            val frame = (progression * (frames.length.toDouble-1)).toInt
            val sprite = frames.apply(frame)
            sprite.position = (Position.x, Position.y)
            window.draw(sprite)
        }
    }

    


abstract class Displayable(val display : Display):
    def getSprite() : Sprite // Donne le sprite de l'objet si il y en a un
    def Draw() : Unit // Dessine l'image de l'objet sur l'écran