import scala.util.Using
import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayDeque
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.Seq

import sfml.graphics.*
import sfml.window.*

val CoutConstructionTrainEnTechnologie = 100
val CoutConstructionBateauEnTechnologie = 400
val CoutConstructionAvionEnTechnologie = 1000

var TransportNewId = 0

// Vitesse en pixel par minute

trait TypeTransport:
    val Nom : String
    val VitesseMax : Double
    val PrixConstruction : Prix
    val TempsArretVille : Double
    def Sprites(display : Display) : ArrayBuffer[Sprite]
    def Switch() : TypeTransport

case class Train() extends TypeTransport:
    val Nom = "Train"
    val VitesseMax = 0.5
    val PrixConstruction: Prix = Prix(Seq[Quantite](Quantite(humain, 8), Quantite(or, 100), Quantite(fer, 80)))
    val TempsArretVille : Double = 5.0
    def Sprites(display : Display) : ArrayBuffer[Sprite] = {
        display.SpritesTrain
    }
    def Switch() : TypeTransport = {
        Bateau()
    }

case class Bateau() extends TypeTransport:
    val Nom = "Bateau"
    val VitesseMax = 0.3
    val PrixConstruction: Prix = Prix(Seq[Quantite](Quantite(humain, 20), Quantite(or, 300), Quantite(bois, 200), Quantite(fer, 50)))
    val TempsArretVille : Double = 20.0
    def Sprites(display : Display) : ArrayBuffer[Sprite] = {
        display.SpritesBateau
    }
    def Switch() : TypeTransport = {
        Avion()
    }

case class Avion() extends TypeTransport:
    val Nom = "Avion"
    val VitesseMax = 1.0
    val PrixConstruction: Prix = Prix(Seq[Quantite](Quantite(humain, 100), Quantite(or, 1000), Quantite(fer, 3000), Quantite(technologie, 2000)))
    val TempsArretVille : Double = 60.0
    def Sprites(display : Display) : ArrayBuffer[Sprite] = {
        display.SpritesAvion
    }
    def Switch() : TypeTransport = {
        Train()
    }



// Contenu des transports selon le niveau de la ville
val CapacitesHumainTransportConstruitsParNiveauVille = Array(0, 3, 15, 100)
val CapacitesRessourceTransportConstruitsParNiveauVille = Array(0, 33, 450, 3000)


class Transport(val Id : Int, val TypeTransport : TypeTransport, val Contenu : Conteneur, var Localisation : Tuile, val world : World) extends Displayable(world.display):

    // Gestion du type de transport
    val VitesseMax : Double = TypeTransport.VitesseMax
    val Nom : String = Contenu match {
        case _ : ConteneurHumain => TypeTransport.Nom
        case _ : ConteneurRessources => TypeTransport.Nom + " marchand"
    }

    // Compute du Sprite
    override def Draw() : Unit = {
        Localisation match {
            case _ : Ville => ()
            case _ => display.Window.draw(getSprite())
        }
    }
    def getSprite() : Sprite = {
        val i = getCodeSprite()
        val sprite = TypeTransport.Sprites(display)(i)
        sprite.position = (Localisation.Position.x, Localisation.Position.y)
        return sprite
    }
    def getCodeSprite() : Int = {
        // 0 : Va vers le haut
        // 1 : Vers la droite
        // 2 : Vers le bas
        // 3 : Vers la gauche
        if !Destinations.isEmpty then {
            val dst = Destinations.apply(0)
            val x = dst.Position.x-Localisation.Position.x
            val y = dst.Position.y-Localisation.Position.y
            if x == 0 then {
                if y < 0 then {
                    0
                }
                else {
                    2
                }
            }
            else {
                if x > 0 then {
                    1
                }
                else {
                    3
                }
            }
        }
        else {
            0
        }
    }

    // Gestions des déplacements
    
    var Destinations : ArrayDeque[Tuile] = ArrayDeque[Tuile]()
    

    var ModeAuto = false
    var VilleChargement : Option[Ville] = None
    var ContenuACharger : Conteneur = Conteneur(Seq[Quantite]())
    var TacheVisee : Option[Tache] = None

    private def ChoixAuto() : Unit = {

        // On choisit une destination avec le meilleur rapport ressource apportée, temps de trajet
        VilleChargement = None
        TacheVisee = None

        var id_t = 0
        while (id_t < world.TableauTaches.length && VilleChargement == None && TacheVisee == None) do {
            var scoreMax = 0.0
            var tempsVoyage = 1.0
            val t = world.TableauTaches.apply(id_t)
            for (i <- 0 to (world.reseau.Hauteur-1)) {
            for (j <- 0 to (world.reseau.Largeur-1)) {
                world.reseau.Grille(i)(j) match {
                    case depart : Ville => {
                        val temps = world.reseau.Dijkstra(this, Localisation, depart)._2 + world.reseau.Dijkstra(this, depart, t.ville)._2
                        val score = ScoreDeRessourceApporter(depart, t).toDouble / temps
                        if score > scoreMax && temps != Double.PositiveInfinity && depart != t.ville then {
                            scoreMax = score
                            tempsVoyage = temps
                            VilleChargement = Some(depart)
                            TacheVisee = Some(t)
                        }
                    }
                    case _ => ()
                }
            }
            }
            id_t = id_t + 1
        }

        

    }
    private def ScoreDeRessourceApporter(ville : Ville, tache : Tache) : Int = {

        // On récupère le prix théorique à payer
        var prix = tache.Prix.PrixRestant(tache.ville.Contenu)
        
        var score = 0
        for p <- prix.Prices do {
            val remplissable = Contenu.LookupCapacite(p.t) - Contenu.Lookup(p.t)
            val disponible = ville.Contenu.Lookup(p.t)
            score = score + Contenu.Lookup(p.t) + min(min(remplissable, disponible), p.q)
        }

        score
        
    }
    private def Charger(ville : Ville, tache : Tache) : Unit = {
        var prix = tache.Prix.PrixRestant(tache.ville.Contenu)
        for (p <- prix.Prices) do {
            var echange = p.q - Contenu.Lookup(p.t)
            echange = Contenu.Ajouter(Quantite(p.t, echange))
            echange = echange - ville.Contenu.Retirer(Quantite(p.t, echange))
            Contenu.Retirer(Quantite(p.t, echange))
        }
    }
    private def Decharger(tache : Tache) : Unit = {
        val ville = tache.ville
        var prix = tache.Prix.PrixRestant(tache.ville.Contenu)
        for (p <- prix.Prices) do {
            var echange = Contenu.Lookup(p.t)
            echange = ville.Contenu.Ajouter(Quantite(p.t, echange))
            Contenu.Retirer(Quantite(p.t, echange))
        }
    }

    private def BasicAvancer() : Unit = {
        
        var dist_a_pracourir = VitesseMax * TimeStepMinute.toDouble
        dist_a_pracourir = Localisation.FaireProgresser(this, dist_a_pracourir)
        while Localisation.AutorisePartir(this) && !Destinations.isEmpty do {
            val nouvelle_localisation = Destinations.apply(0)
            if (nouvelle_localisation.AutoriseArriver(this)) then {
                Localisation.FairePartir(this)
                Localisation = nouvelle_localisation
                nouvelle_localisation.FaireArriver(this)
                Destinations.remove(0)
                dist_a_pracourir = Localisation.FaireProgresser(this, dist_a_pracourir)
            }
            else {
                return ()
            }
        }

    }

    def Avancer() : Unit = {

        if ModeAuto && Destinations.isEmpty then {
            (TacheVisee, VilleChargement) match {
                case (Some(tache), Some(ville)) if Localisation == ville => {
                    // Le chargement n'a pas été fait
                    Charger(ville, tache)
                    VilleChargement = None
                }
                case (Some(_), Some(ville)) => {
                    // On doit se rendre dans la ville
                    Destinations = world.reseau.Dijkstra(this, Localisation, ville)._1
                }
                case (Some(tache), None) if Localisation == tache.ville => {
                    // On doit décharger
                    Decharger(tache)
                    TacheVisee = None
                }
                case (Some(tache), None) => {
                    // On doit se rendre dans la ville de la tache
                    Destinations = world.reseau.Dijkstra(this, Localisation, tache.ville)._1
                }
                case (None, None) => {
                    ChoixAuto()
                }
            }
        }

        BasicAvancer()
        
    }