import scala.util.Using
import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayDeque
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.Seq

import sfml.graphics.*
import sfml.window.*

val CapacitePopulationParNiveau = Array(0, 10000, 20000, 40000, 100000, 300000, 1000000)

val CoutConstructionCheminDeFer = 50

val CapaciteEnRessourceParNiveau = Array(0, 100000, 200000, 400000, 1000000, 3000000, 10000000)

val TauxDeProductionParNiveauParHabitant = Array(0.0, 0.03, 0.04, 0.05)
val TauxDeNataliteParNiveau = Array(0.0, 0.001, 0.002, 0.005)

trait TypeVille:
    val Nom : String
    val Capacites : Array[Seq[Quantite]]
    val PrixEvolutions : Array[Prix]
    val TauxDeProductions : Array[Double]
    val TransportsConstructibles : HashMap[TypeTransport, Boolean]
    val typeRessourceProduite : TypeQuantite
    val Sprites : ArrayBuffer[Sprite]

class VilleMiniere(world : World) extends TypeVille:
    val Nom = "Ville Miniere"
    val typeRessourceProduite : TypeQuantite = fer
    val Capacites = Array(
        Seq[Quantite](),
        Seq[Quantite](Quantite(humain, 10), Quantite(technologie, 100), Quantite(bois, 100), Quantite(pierre, 100), Quantite(fer, 100), Quantite(poisson, 100)),
        Seq[Quantite](Quantite(humain, 50), Quantite(technologie, 1000), Quantite(bois, 1000), Quantite(pierre, 1000), Quantite(fer, 1000), Quantite(poisson, 1000)),
        Seq[Quantite](Quantite(humain, 300), Quantite(technologie, 7000), Quantite(bois, 7000), Quantite(pierre, 7000), Quantite(fer, 7000), Quantite(poisson, 7000))
    )
    val PrixEvolutions = Array(
        Prix(Seq[Quantite](Quantite(or, 100))),
        Prix(Seq[Quantite](Quantite(humain, 6), Quantite(bois, 50))),
        Prix(Seq[Quantite](Quantite(humain, 40), Quantite(pierre, 50)))
    )
    val TauxDeProductions = Array(0.0, 0.03, 0.1, 0.5)
    val TransportsConstructibles = HashMap[TypeTransport, Boolean]((Train(), true))
    val Sprites : ArrayBuffer[Sprite] = world.display.SpritesVilleMiniere

class VillePortuaire(world : World) extends TypeVille:
    val Nom = "Ville Portuaire"
    val typeRessourceProduite : TypeQuantite = poisson
    val Capacites = Array(
        Seq[Quantite](),
        Seq[Quantite](Quantite(humain, 10), Quantite(technologie, 100), Quantite(bois, 100), Quantite(pierre, 100), Quantite(fer, 100), Quantite(poisson, 100)),
        Seq[Quantite](Quantite(humain, 50), Quantite(technologie, 1000), Quantite(bois, 1000), Quantite(pierre, 1000), Quantite(fer, 1000), Quantite(poisson, 1000)),
        Seq[Quantite](Quantite(humain, 300), Quantite(technologie, 7000), Quantite(bois, 7000), Quantite(pierre, 7000), Quantite(fer, 7000), Quantite(poisson, 7000))
    )
    val PrixEvolutions = Array(
        Prix(Seq[Quantite](Quantite(or, 300))),
        Prix(Seq[Quantite](Quantite(humain, 6), Quantite(bois, 50))),
        Prix(Seq[Quantite](Quantite(humain, 40), Quantite(pierre, 50)))
    )
    val TauxDeProductions = Array(0.0, 0.03, 0.1, 0.5)
    val TransportsConstructibles = HashMap[TypeTransport, Boolean]((Bateau(), true))
    val Sprites : ArrayBuffer[Sprite] = world.display.SpritesVillePortuaire

class VilleScierie(world : World) extends TypeVille:
    val Nom = "Ville Scierie"
    val typeRessourceProduite : TypeQuantite = bois
    val Capacites = Array(
        Seq[Quantite](),
        Seq[Quantite](Quantite(humain, 10), Quantite(technologie, 100), Quantite(bois, 100), Quantite(pierre, 100), Quantite(fer, 100), Quantite(poisson, 100)),
        Seq[Quantite](Quantite(humain, 50), Quantite(technologie, 1000), Quantite(bois, 1000), Quantite(pierre, 1000), Quantite(fer, 1000), Quantite(poisson, 1000)),
        Seq[Quantite](Quantite(humain, 300), Quantite(technologie, 7000), Quantite(bois, 7000), Quantite(pierre, 7000), Quantite(fer, 7000), Quantite(poisson, 7000))
    )
    val PrixEvolutions = Array(
        Prix(Seq[Quantite](Quantite(or, 1000))),
        Prix(Seq[Quantite](Quantite(humain, 6), Quantite(bois, 50))),
        Prix(Seq[Quantite](Quantite(humain, 40), Quantite(pierre, 50)))
    )
    val TauxDeProductions = Array(0.0, 0.03, 0.1, 0.5)
    val TransportsConstructibles = HashMap[TypeTransport, Boolean]((Train(), true))
    val Sprites : ArrayBuffer[Sprite] = world.display.SpritesVilleScierie

class VilleCarrierre(world : World) extends TypeVille:
    val Nom = "Ville Carriere"
    val typeRessourceProduite : TypeQuantite = pierre
    val Capacites = Array(
        Seq[Quantite](),
        Seq[Quantite](Quantite(humain, 10), Quantite(technologie, 100), Quantite(bois, 100), Quantite(pierre, 100), Quantite(fer, 100), Quantite(poisson, 100)),
        Seq[Quantite](Quantite(humain, 50), Quantite(technologie, 1000), Quantite(bois, 1000), Quantite(pierre, 1000), Quantite(fer, 1000), Quantite(poisson, 1000)),
        Seq[Quantite](Quantite(humain, 300), Quantite(technologie, 7000), Quantite(bois, 7000), Quantite(pierre, 7000), Quantite(fer, 7000), Quantite(poisson, 7000))
    )
    val PrixEvolutions = Array(
        Prix(Seq[Quantite](Quantite(or, 1000))),
        Prix(Seq[Quantite](Quantite(humain, 6), Quantite(bois, 50))),
        Prix(Seq[Quantite](Quantite(humain, 40), Quantite(pierre, 50)))
    )
    val TauxDeProductions = Array(0.0, 0.03, 0.1, 0.5)
    val TransportsConstructibles = HashMap[TypeTransport, Boolean]((Train(), true))
    val Sprites : ArrayBuffer[Sprite] = world.display.SpritesVilleCarriere

class VilleTechnologique(world : World) extends TypeVille:
    val Nom = "Ville Technologique"
    val typeRessourceProduite : TypeQuantite = technologie
    val Capacites = Array(
        Seq[Quantite](),
        Seq[Quantite](Quantite(humain, 10), Quantite(technologie, 100), Quantite(bois, 100), Quantite(pierre, 100), Quantite(fer, 100), Quantite(poisson, 100)),
        Seq[Quantite](Quantite(humain, 50), Quantite(technologie, 1000), Quantite(bois, 1000), Quantite(pierre, 1000), Quantite(fer, 1000), Quantite(poisson, 1000)),
        Seq[Quantite](Quantite(humain, 300), Quantite(technologie, 7000), Quantite(bois, 7000), Quantite(pierre, 7000), Quantite(fer, 7000), Quantite(poisson, 7000))
    )
    val PrixEvolutions = Array(
        Prix(Seq[Quantite](Quantite(or, 14000))),
        Prix(Seq[Quantite](Quantite(humain, 6), Quantite(bois, 50))),
        Prix(Seq[Quantite](Quantite(humain, 40), Quantite(pierre, 50)))
    )
    val TauxDeProductions = Array(0.0, 0.03, 0.1, 0.5)
    val TransportsConstructibles = HashMap[TypeTransport, Boolean]((Avion(), true), (Train(), true))
    val Sprites : ArrayBuffer[Sprite] = world.display.SpritesVilleTechnologique


class Ville(val TypeVille : TypeVille, override val Position : Vec2, override val world : World) extends TuileVille(Position, world):

    // Caractéristiques
    var Niveau : Int = 1
    var Contenu : Conteneur = Conteneur(TypeVille.Capacites(1))
    Contenu.Ajouter(Quantite(humain, 2))

    override def getSprite() : Sprite = {
        TypeVille.Sprites(EtatTuile.CodeSpriteVille).position = (Position.x, Position.y)
        TypeVille.Sprites(EtatTuile.CodeSpriteVille)
    }

    // Actions sur elle même
    def ActionEvoluer(): Boolean = { // Teste la présence des ressource pour augmenter le niveau de la ville
        if Niveau < 3 then {
            if (TypeVille.PrixEvolutions(Niveau).PayWithErrorDisplay(Contenu, display)) {
                Niveau = Niveau + 1
                val AncienConteneur = Contenu
                Contenu = Conteneur(TypeVille.Capacites(Niveau))
                Contenu.Ajouter(AncienConteneur)
                return true
            }
        }
        return false
    }
    def ActionProduireUniversel(ressourceType : TypeQuantite) : Unit = {

        var prob = ((Contenu.Lookup(humain) + Contenu.Lookup(technologie)).toDouble * TauxDeProductionParNiveauParHabitant(Niveau) / 2.0) * TimeStepMinute.toDouble
        var q = 0
        if prob <= 1.0 then {
            if Random.between(0.0, 1.0) <= prob then {
                q = 1
            }
        }
        else {
            q = prob.toInt
        }
        q = Contenu.Ajouter(Quantite(ressourceType, q));

        prob = (Contenu.Lookup(humain).toDouble * TauxDeNataliteParNiveau(Niveau)) * TimeStepMinute.toDouble
        var humains = 0 
        if prob <= 1.0 then {
            if Random.between(0.0, 1.0) <= prob then {
                humains = 1
            }
        }
        else {
            humains = prob.toInt
        }
        q = Contenu.Ajouter(Quantite(humain, humains));
    }
    def ActionProduire(): Unit = {
        EtatTuile match {
            case _ : EtatNormal => ActionProduireUniversel(TypeVille.typeRessourceProduite)
            case _ => ()
        }
    }

    override def getSelector() : SelectorTuile = {
        SelectorVille(this)
    }

    override def Update() : Unit = {
        ProgressionDesTransports.foreach(e => e._1.Avancer())
        EtatTuile.Agir()
        ActionProduire()
        EtatTuile match {
            case _ : EtatNormal => {
                if (Random.between(0.0, 1.0) <= ProbEnvahissementParLaNature*TimeStepMinute.toDouble) then {
                    ChangerEtatTuile(EnvahiParLaNature(this))
                }
            }
            case _ => ()
        }
    }

    def ViderContenu() : Unit = {
        Contenu = Conteneur(TypeVille.Capacites(Niveau))
    }
    