import scala.util.Using
import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayDeque
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.Seq

import sfml.graphics.*
import sfml.window.*

def VoronoiMap(Hauteur : Int, Largeur : Int, Points : Array[(Int, Int, Nature)], world : World) = {
    val res = Array.ofDim[Nature](Hauteur, Largeur)
    for (i <- 0 to (Hauteur-1)) {
        for (j <- 0 to (Largeur-1)) {
            var env : Nature = Eau(world)
            var distMin = Hauteur*Hauteur + Largeur*Largeur + 1
            Points.foreach(point => {
                val tmp : Int = DistAuCarre(Vec2(i, j), Vec2(point._1, point._2)).toInt
                if tmp < distMin then {
                    distMin = tmp
                    env = point._3
                }
            })
            res(i)(j) = env
        }
    }
    res
}


class Reseau(val Hauteur : Int, val Largeur : Int, val world : World):

    val display = world.display

    val Grille = Array.ofDim[Tuile](Hauteur, Largeur)
    {

        val zone_number = Random.between((Hauteur+Largeur)/2, Hauteur+Largeur)
        val points = Array.ofDim[(Int, Int, Nature)](zone_number.toInt)
        for (i <- 0 to (zone_number-1).toInt) {
            val x = Random.between(0, Hauteur)
            val y = Random.between(0, Largeur)
            val env = EnvNaturelsByNumber(world)(Random.between(0, NbEnvNaturels))
            points(i) = (x, y, env)
        }
        val plan = VoronoiMap(Hauteur, Largeur, points, world)

        for (i <- 0 to (Hauteur-1)) {
            for (j <- 0 to (Largeur-1)) {
                Grille(i)(j) = TuileEnvironnement(plan(i)(j), Vec2(i*32, j*32), world)
            }
        }

    }

    // Trouver le chemin pour un transport

    def AccessiblesDepuis(tuile : Tuile) : ArrayBuffer[Tuile] = {
        val i = tuile.Position.x/32
        val j = tuile.Position.y/32
        val res = ArrayBuffer[Tuile]()
        if i > 0 then res.addOne(Grille(i-1)(j))
        if i < (Hauteur-1) then res.addOne(Grille(i+1)(j))
        if j > 0 then res.addOne(Grille(i)(j-1))
        if j < (Largeur-1) then res.addOne(Grille(i)(j+1))
        res
    }

    object OrderPrioQ extends Ordering[(Tuile, Tuile, Double)]{
        def compare(a:(Tuile, Tuile, Double), b:(Tuile, Tuile, Double)) = b._3.compare(a._3)
    }
    def Dijkstra(transport : Transport, Depart : Tuile, Arrivee : Tuile) : (ArrayDeque[Tuile], Double) = { // Donne le chemin le plus court géographiquement entre deux Portion pour un train donné
        println("Appel a dijktra !")
        val d = HashMap[Tuile, Double]()
        val prec = HashMap[Tuile, Tuile]()

        val prioQ = PriorityQueue[(Tuile, Tuile, Double)]((Depart, Depart, Depart.TempsMin(transport)))(OrderPrioQ)

        while !prec.contains(Arrivee) && prioQ.nonEmpty do {
            val s = prioQ.dequeue()
            if !prec.contains(s._2) then {
                prec.addOne((s._2, s._1))
                d(s._2) = s._3
                AccessiblesDepuis(s._2).foreach(suiv => {
                    val poid = s._3 + suiv.TempsMin(transport)
                    if !prec.contains(suiv) && (!d.contains(suiv) || d.apply(suiv) > poid) then {
                        d.addOne((suiv, poid))
                        prioQ.addOne((s._2, suiv, poid))
                    }
                })
            }
        }
        if d.apply(Arrivee) != Double.PositiveInfinity then {
            val res = ArrayDeque[Tuile](Arrivee)
            while prec.apply(res.apply(0)) != res.apply(0) do {
                res.insert(0, prec.apply(res.apply(0)))
            }
            (res, d.apply(Arrivee))
                
        }
        else {
            (ArrayDeque[Tuile](), Double.PositiveInfinity)
        }

    }

    // Dessiner la map
    def Draw() : Unit = {
        for (i <- 0 to (Hauteur-1)) {
            for (j <- 0 to (Largeur-1)) {
                Grille(i)(j).Draw()
                Grille(i)(j).ProgressionDesTransports.foreach(e => e._1.Draw())
            }
        }
    }

    // Mettre à jour la map
    def Update() : Unit = {
        for (i <- 0 to (Hauteur-1)) {
            for (j <- 0 to (Largeur-1)) {
                Grille(i)(j).Update()
            }
        }
    }