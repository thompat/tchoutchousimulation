import scala.util.Using
import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayDeque
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.Seq

import sfml.graphics.*
import sfml.window.*

abstract class BoutonTexte(val Position : Vec2, var Texte : String, override val display : Display) extends Displayable(display) with Cliquable:

    def EstClique(Localisation : Vec2) : Boolean = {
        display.text.position = (Position.x, Position.y)
        display.text.string = Texte
        val bounds = display.text.globalBounds
        bounds.contains(sfml.system.Vector2[Float](Localisation.x.toFloat, Localisation.y.toFloat))
    }

    override def getSprite(): Sprite = {
        display.SpriteArgent // On met n'importe quoi
    }
    override def Draw(): Unit = {
        Actualise()
        display.text.position = (Position.x, Position.y)
        display.text.string = Texte
        display.Window.draw(display.text)
    }
    def Actualise() : Unit = ()

    def Action() : Unit = {()}
    def RouletteSouris(delta : Float) : Unit = {()}
    def CliqueDroit() : Unit = {()}

case class EchangeQuantiteTransportVille(val quantiteType : Quantite, val ville : Ville, val transport : Transport, override val Position : Vec2, override val display : Display) extends BoutonTexte(Position, f"${transport.Contenu.Lookup(quantiteType.t)}/${ville.Contenu.Lookup(quantiteType.t)}", display):
    override def RouletteSouris(delta: Float): Unit = {
        quantiteType.q = 1
        if delta > 0 then {
            quantiteType.q = ville.Contenu.Retirer(quantiteType)
            quantiteType.q = quantiteType.q - transport.Contenu.Ajouter(quantiteType)
            ville.Contenu.Ajouter(quantiteType);
        }
        else {
            quantiteType.q = ville.Contenu.Ajouter(quantiteType)
            quantiteType.q = quantiteType.q - transport.Contenu.Retirer(quantiteType)
            ville.Contenu.Retirer(quantiteType);
        }
    }
    override def Actualise(): Unit = {
        Texte = f"${transport.Contenu.Lookup(quantiteType.t)}/${ville.Contenu.Lookup(quantiteType.t)}"
    }

case class BoutonVendreRessource(val ville : Ville, override val Position : Vec2, override val display : Display) extends BoutonTexte(Position, f"Vendre 10 Technologies", display):
    var typeQuantite : Ressource = technologie
    var q = 10
    override def Actualise() : Unit = {
        Texte = f"Vendre ${q} "+typeQuantite.Nom+"s"
    }
    override def Action() : Unit = {
        val tmp = ville.Contenu.Retirer(Quantite(typeQuantite, q));
        Argent = Argent + PrixRessource(Quantite(typeQuantite, tmp), ville)
    }
    override def RouletteSouris(delta: Float): Unit = {
        if delta > 0 then {
            q = q + 1
        }
        else if q > 0 then {
            q = q - 1
        }
    }
    override def CliqueDroit() : Unit = {
        typeQuantite = typeQuantite.Switch()
    }

case class BoutonConstruireVille(val world : World, val tuile : TuileEnvironnement, override val Position : Vec2, override val display : Display) extends BoutonTexte(Position, f"Construire une Ville", display):
    override def Action() : Unit = {

        val Grille = world.reseau.Grille
        val i = tuile.Position.x/32
        val j = tuile.Position.y/32

        tuile.TypeEnvironnement match {
            case env_nat : Nature => {
                env_nat.VilleAConstruire(tuile) match {
                    case Some(tville) => {
                        if tville.PrixEvolutions(0).PayWithErrorDisplay(Conteneur(Seq[Quantite]()), world.display) then {
                            Grille(i)(j) = Ville(tville, tuile.Position, world)
                        }
                    }
                    case None => display.PrintInfo("Impossible de constuire sur cette parcelle")
                }
            }
            case _ => {
                display.PrintInfo("Impossible de constuire sur une parcelle déjà construite")
            }
        }

        world.ui.RetirerSelector()

    }

case class BoutonConstruireConstruction(val world : World, val tuile : Tuile, override val Position : Vec2, override val display : Display) extends BoutonTexte(Position, f"Construire Chemin De Fer", display):
    var Construction : Construction = CheminDeFer(world)
    override def Actualise() : Unit = {
        Texte = f"Constuire ${Construction.Nom}"
    }
    override def Action() : Unit = {

        val Grille = world.reseau.Grille
        val i = tuile.Position.x/32
        val j = tuile.Position.y/32

        if Construction.PrixConstruction.PayWithErrorDisplay(Conteneur(Seq[Quantite]()), world.display) then {
            val newTile = TuileEnvironnement(Construction, tuile.Position, world)
            Grille(i)(j) = newTile
        }

        world.ui.RetirerSelector()

    }
    override def CliqueDroit() : Unit = {
        Construction = Construction.Switch(world)
    }

case class BoutonFaireEvoluerVille(val ville : Ville, override val Position : Vec2, override val display : Display) extends BoutonTexte(Position, f"Faire evoluer la ville", display):
    override def Action() : Unit = {
        ville.world.NouvelleTache(TacheEvoluerVille(ville))
    }

case class BoutonConstruireTransport(val ville : Ville, override val Position : Vec2, override val display : Display) extends BoutonTexte(Position, "", display):
    var Conteneur : Conteneur = ConteneurHumain(CapacitesHumainTransportConstruitsParNiveauVille(ville.Niveau))
    var TypeTransport : TypeTransport = Train()
    while !(ville.TypeVille.TransportsConstructibles.contains(TypeTransport)) do {
        TypeTransport = TypeTransport.Switch()
    }

    override def Actualise() : Unit = {
        val transport = Transport(0, TypeTransport, Conteneur, ville, ville.world)
        Texte = f"Construire ${transport.Nom}"
    }

    override def Action() : Unit = {
        ville.world.NouvelleTache(TacheConstruireTransport(ville, TypeTransport, Conteneur))
    }
    override def RouletteSouris(delta: Float): Unit = {
        TypeTransport = TypeTransport.Switch()
        while !(ville.TypeVille.TransportsConstructibles.contains(TypeTransport)) do {
            TypeTransport = TypeTransport.Switch()
        }
    }
    override def CliqueDroit() : Unit = {
        Conteneur match {
            case _  : ConteneurHumain => Conteneur = ConteneurRessources(CapacitesRessourceTransportConstruitsParNiveauVille(ville.Niveau))
            case _  => Conteneur = ConteneurHumain(CapacitesHumainTransportConstruitsParNiveauVille(ville.Niveau))
        }
    }

case class BoutonSelectionnerTransport(val ui : UI, val transport : Transport, override val Position : Vec2, override val display : Display) extends BoutonTexte(Position, f"${transport.Nom} ${transport.Id}", display):
    override def Action() : Unit = {
        ui.AddSelector(SelectorTransport(transport))
    }

case class BoutonSelectionner(val ui : UI, val Selection : Selector, txt : String, override val Position : Vec2, override val display : Display) extends BoutonTexte(Position, txt, display):
    override def Action() : Unit = {
        ui.AddSelector(Selection)
    }

case class BoutonRetour(val ui : UI, override val Position : Vec2, override val display : Display) extends BoutonTexte(Position, "Retour", display):
    override def Action() : Unit = {
        ui.RetirerSelector()
    }

case class BoutonAction(val action : () => Unit, txt : String, override val Position : Vec2, override val display : Display) extends BoutonTexte(Position, txt, display):
    override def Action() = action()

def CreateBoutonReparer(tuile : Tuile, prix : Prix, pos : Vec2, display : Display) : BoutonAction = {
    val action = () => {
        if prix.PayWithErrorDisplay(Conteneur(Seq[Quantite]()), display) then {
            tuile.ChangerEtatTuile(EtatNormal(tuile))
        }
    }
    return BoutonAction(action, "Reparer", pos, display)
}

def CreateBoutonDeclencherFeu(tuile : Tuile, pos : Vec2, display : Display) : BoutonAction = {
    val action = () => {
        tuile.ChangerEtatTuile(EnFeu(tuile))
    }
    return BoutonAction(action, "Allumer le feu !!", pos, display)
}