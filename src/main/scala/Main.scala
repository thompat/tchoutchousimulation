import scala.util.Using
import scala.util.Random
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayDeque
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.Seq

import sfml.graphics.*
import sfml.window.*


val TimeStepMinute = 1;

class World(use : Using.Manager):

    val window : RenderWindow = use(RenderWindow(VideoMode(832, 800), "TchoutchouSimulation"))
    window.framerateLimit = 30

    val display : Display = Display(window, use)
    val reseau : Reseau = Reseau(26, 25, this)

    private var NewIdAnim = 0
    var Animations : HashMap[Int, Animation] = HashMap[Int, Animation]()
    def AjouterAnimation(anim : Animation) : Int = {
        Animations.addOne((NewIdAnim, anim))
        NewIdAnim = NewIdAnim + 1
        NewIdAnim - 1
    }
    def RetirerAnimation(IdAnim : Int) : Unit = {
        Animations.remove(IdAnim)
    }
    def Animate() : Unit = {

        // On doit faire en sorte que les animations les plus en bas de l'écran soient devant les autres
        val classifieur = HashMap[Int, List[Animation]]()

        Animations.foreach((id, anim) => {
            if anim.ended then Animations.remove(id) else ()
            classifieur.get(-anim.Position.y) match {
                case None => classifieur.addOne((-anim.Position.y, anim :: Nil))
                case Some(lst) => classifieur.addOne((-anim.Position.y, anim :: lst))
            }
            anim.Draw(window)
            anim.Next()
        })

        classifieur.foreach((_, lst) => lst.foreach(anim => {
            anim.Draw(window)
            anim.Next()
        }))

    }
    

    val textArgent = use(Text())
    textArgent.position = (23*32, 8)
    textArgent.font = display.font
    textArgent.characterSize = 16
    textArgent.fillColor = Color.White()

    val ui : UI = UI(this)

    val TableauTaches : ArrayDeque[Tache] = ArrayDeque[Tache]()
    var IndexTableauTaches : HashMap[Tache, Int] = HashMap[Tache, Int]()
    def NouvelleTache(t : Tache) : Unit = {
        if (IndexTableauTaches.contains(t)) then {
            display.PrintInfo("La tache est deja presente dans la liste des tache a faires")
        }
        else {
            display.PrintInfo("La tache a bien ete ajoute")
            TableauTaches.insert(0, t)
            UpdateIndexTableauTaches()
        }
    }
    def UpdateIndexTableauTaches() : Unit = {
        IndexTableauTaches = HashMap[Tache, Int]()
        var i = 0
        TableauTaches.foreach(t => {
            IndexTableauTaches.addOne((t, i))
            i = i + 1
        })
    }
    def EffectuerTaches() : Unit = {
        var i = 0
        while i < TableauTaches.length do {
            val t = TableauTaches.apply(i)
            if t.Essaie() then {
                TableauTaches.remove(i)
            }
            else {
                i = i + 1
            }
        }
        UpdateIndexTableauTaches()
    }

    def AfficherArgent() : Unit = {
        display.SpriteArgent.position = (22*32, 0)
        window.draw(display.SpriteArgent)
        textArgent.string = f"${Argent}"
        window.draw(textArgent)
    }

    def Play() : Unit = {

        /* Start the game loop */
            while window.isOpen() do

                /* Clear the screen */
                window.clear()

                reseau.Draw()
                Animate()
                ui.Interagir()
                AfficherArgent()
                display.AfficherInfo()
                

                window.display()

                EffectuerTaches()
                reseau.Update()

                

    }












@main def main =

    Using.Manager { use => 
        val world = World(use)
        world.Play()
    }.get
    
    System.exit(0)
