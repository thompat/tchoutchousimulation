# Ignore Scala Native's MemoryPool's leak
leak:_SM36scala.scalanative.runtime.MemoryPoolD13allocateChunkuEPT36scala.scalanative.runtime.MemoryPool

# Ignore drivers' leaks
leak:drivers/iigd_dch
leak:dri
